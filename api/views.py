import json

from django.http import QueryDict
from django.shortcuts import render
from rest_framework.parsers import FileUploadParser, JSONParser, MultiPartParser
from rest_framework import status
from rest_framework.views import APIView
from rest_framework import generics
from serializers import SearchSerializer, DangerSerializer, SafeContactSerializer, NotificationSerializer,\
    ApprovesSerializer, RegisterSerializer, PersonDetailSerializer, ApproveDelSerializer, FileUploadSerializer
from models import Person, SafeContact, Notification, ApproveRequest, FileUpload
from django.contrib.auth.models import User
from rest_framework.response import Response
# from rest_framework.authentication import SessionAuthentication
from rest_framework.authtoken.models import Token
# Create your views here.


class Search(generics.ListAPIView):
    serializer_class = SearchSerializer

    def get_queryset(self):
        name = self.request.GET['name']
        users = User.objects.filter(first_name__icontains=name).values_list('id', flat=True)
        persons = Person.objects.filter(user=users)
        return persons


class Contact(generics.ListCreateAPIView):
    serializer_class = SafeContactSerializer

    def perform_create(self, serializer):
        ar = ApproveRequest()
        ar.person_asked = Person.objects.get(user_id=self.request.POST.get('related_contact'))
        ar.person_asking = Person.objects.get(user_id=self.request.POST.get('for_who'))
        ar.save()
        serializer.save()

    def get_queryset(self):
        pk = self.kwargs['pk']
        return SafeContact.objects.filter(for_who=pk)
        # return


def create_notifications(pk):
    related_ids = SafeContact.objects.filter(for_who=pk).values_list('related_contact', flat=True)
    for related_id in related_ids:
        if len(Notification.objects.filter(friend_in_danger=pk, send_to=related_id)) == 0:
            notification = Notification()
            notification.friend_in_danger = Person.objects.get(user_id=pk)
            notification.send_to = Person.objects.get(user_id=related_id)
            notification.save()


def destroy_notifications(pk):
    Notification.objects.filter(friend_in_danger=pk).delete()


class Danger(generics.UpdateAPIView):
    serializer_class = DangerSerializer

    def get_queryset(self):
        pk = self.kwargs['pk']
        in_danger = self.request.data['in_danger']
        if in_danger == "true":
            create_notifications(pk)
        else:
            destroy_notifications(pk)
        return Person.objects.filter(user_id=pk)


class Notifications(generics.ListAPIView):
    serializer_class = NotificationSerializer

    def get_queryset(self):
        send_to = self.kwargs['send_to']
        return Notification.objects.filter(send_to=send_to)


class Approves(generics.ListAPIView):
    serializer_class = ApprovesSerializer

    def get_queryset(self):
        person_asked = Person.objects.get(pk=self.kwargs['person_asked'])
        return ApproveRequest.objects.filter(person_asked=person_asked)


# class CsrfExemptSessionAuthentication(SessionAuthentication):
#
#     def enforce_csrf(self, request):
#         return  # To not perform the csrf check previously happening


class Register(generics.CreateAPIView):
    serializer_class = RegisterSerializer

    # def perform_create(self, serializer):
    #     new_username = serializer.validated_data['username']
    #     serializer.save()
    #     new_user = User.objects.get(username=new_username)
    #     Token.objects.create(user=new_user)


class PersonDetail(generics.RetrieveAPIView):
    serializer_class = PersonDetailSerializer

    def get_queryset(self):
        return Person.objects.filter(user_id=self.kwargs['pk'])


class ApproveSomeone(APIView):
    # serializer_class = ApproveDelSerializer
    # for_who = person_asking
    # related_contect = person_asked

    def post(self, request):
        data = json.loads(request.body)
        for_who = data['person_asking']
        related_contact = data['person_asked']
        approved = data['is_approved']
        contacts = SafeContact.objects.filter(for_who=for_who, related_contact=related_contact)
        for contact in contacts:
            if approved == "true":
                contact.is_approved = True
            else:
                contact.is_approved = False
            contact.save()
        ApproveRequest.objects.filter(person_asked=related_contact, person_asking=for_who).delete()

        return Response({"success": True})


class UploadProfileImage(generics.CreateAPIView, generics.RetrieveAPIView):
    serializer_class = FileUploadSerializer
    queryset = FileUpload.objects.all()
    lookup_field = 'related_person'


