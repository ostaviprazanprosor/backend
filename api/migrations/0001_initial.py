# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0006_require_contenttypes_0002'),
    ]

    operations = [
        migrations.CreateModel(
            name='ApproveRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
            ],
        ),
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('time', models.DateTimeField(default=datetime.datetime.now)),
            ],
        ),
        migrations.CreateModel(
            name='Person',
            fields=[
                ('user', models.OneToOneField(related_name='profile', primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('in_danger', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='SafeContact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('is_approved', models.BooleanField(default=False)),
                ('for_who', models.ForeignKey(related_name='for_who', to='api.Person')),
                ('related_contact', models.ForeignKey(related_name='related', to='api.Person')),
            ],
        ),
        migrations.CreateModel(
            name='Travel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('travel_from_lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('travel_from_lng', models.DecimalField(max_digits=9, decimal_places=6)),
                ('travel_to_lat', models.DecimalField(max_digits=9, decimal_places=6)),
                ('travel_to_lng', models.DecimalField(max_digits=9, decimal_places=6)),
                ('time', models.DateTimeField(default=datetime.datetime.now)),
                ('traveler', models.ForeignKey(related_name='traveler', to='api.Person')),
            ],
        ),
        migrations.AddField(
            model_name='notification',
            name='friend_in_danger',
            field=models.ForeignKey(related_name='friend_in_danger', to='api.Person'),
        ),
        migrations.AddField(
            model_name='notification',
            name='send_to',
            field=models.ForeignKey(related_name='send_to', to='api.Person'),
        ),
        migrations.AddField(
            model_name='approverequest',
            name='person_asked',
            field=models.ForeignKey(related_name='person_asked', to='api.Person'),
        ),
        migrations.AddField(
            model_name='approverequest',
            name='person_asking',
            field=models.ForeignKey(related_name='person_asking', to='api.Person'),
        ),
    ]
