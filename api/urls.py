from django.conf.urls import include, url
from rest_framework.urlpatterns import format_suffix_patterns
import views
from rest_framework.authtoken import views as auth_views
import tokenViews


urlpatterns = [
    url(r'^token-auth/', tokenViews.obtain_auth_token),
    url(r'^search/', views.Search.as_view(), name='search'),
    url(r'^contact/(?P<pk>[0-9]*)', views.Contact.as_view(), name='contact'),
    url(r'^danger/(?P<pk>[0-9]+)', views.Danger.as_view(), name='danger'),
    url(r'^notifications/(?P<send_to>[0-9]+)', views.Notifications.as_view(), name='notifications'),
    url(r'^pendingApprovals/(?P<person_asked>[0-9]+)', views.Approves.as_view(), name='approves'),
    url(r'^approve/', views.ApproveSomeone.as_view(), name='approve_someone'),
    url(r'^register/', views.Register.as_view(), name='register'),
    url(r'^personDetail/(?P<pk>[0-9]+)/', views.PersonDetail.as_view(), name='person_detail'),
    url(r'^uploadProfileImage/', views.UploadProfileImage.as_view(), name='image_upload'),

]

# urlpatterns = format_suffix_patterns(urlpatterns)