from django.db import models
from django.contrib.auth.models import User
import datetime

# Create your models here.


class Person(models.Model):
    user = models.OneToOneField(User, primary_key=True, related_name='profile')
    in_danger = models.BooleanField(default=False)

    def __unicode__(self):
        return "User: " + self.user.username + " id: " + str(self.user.id)


class SafeContact(models.Model):
    for_who = models.ForeignKey(Person, related_name='for_who')
    related_contact = models.ForeignKey(Person, related_name='related')
    is_approved = models.BooleanField(default=True)

    def __unicode__(self):
        return "For " + str(self.for_who) + " related to: " + str(self.related_contact)


class Travel(models.Model):
    traveler = models.ForeignKey(Person, related_name='traveler')
    travel_from_lat = models.DecimalField(max_digits=9, decimal_places=6)
    travel_from_lng = models.DecimalField(max_digits=9, decimal_places=6)
    travel_to_lat = models.DecimalField(max_digits=9, decimal_places=6)
    travel_to_lng = models.DecimalField(max_digits=9, decimal_places=6)
    time = models.DateTimeField(default=datetime.datetime.now)


class Notification(models.Model):
    friend_in_danger = models.ForeignKey(Person, related_name='friend_in_danger')
    send_to = models.ForeignKey(Person, related_name='send_to')
    time = models.DateTimeField(default=datetime.datetime.now)

    def __unicode__(self):
        return "In danger: " + str(self.friend_in_danger) + " send to: " + str(self.send_to)


class ApproveRequest(models.Model):
    person_asking = models.ForeignKey(Person, related_name='person_asking')
    person_asked = models.ForeignKey(Person, related_name='person_asked')

    def __unicode__(self):
        return "Asking: " + str(self.person_asking) + ", asked: " + str(self.person_asked)


class FileUpload(models.Model):
    date_created = models.DateTimeField(auto_now_add=True)
    image_location = models.ImageField(max_length=250, upload_to='api/static/images')
    related_person = models.ForeignKey(Person, related_name='profile_image')