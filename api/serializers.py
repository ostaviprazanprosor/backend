from rest_framework import serializers
from models import Person, SafeContact, Notification, ApproveRequest, FileUpload
from django.contrib.auth.models import User


class UserIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', )


class UserSearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'id')


class SearchSerializer(serializers.ModelSerializer):
    user = UserSearchSerializer()

    class Meta:
        model = Person
        fields = ('user',)


class DangerSerializer (serializers.ModelSerializer):
    class Meta:
        model = Person
        fields = ('in_danger',)


class SafeContactSerializer(serializers.ModelSerializer):
    class Meta:
        model = SafeContact
        fields = ('for_who', 'related_contact', 'is_approved')


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('time', 'friend_in_danger')


class ApprovesSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApproveRequest
        fields = ('person_asking', 'person_asked')


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'username', 'password', 'email')
        write_only_fields = ('password',)


class RegisterSerializer(serializers.ModelSerializer):
    user = UserSerializer()

    class Meta:
        model = Person
        fields = ('user', 'in_danger')

    def create(self, validated_data):
        in_danger = validated_data.pop('in_danger')
        arg = validated_data['user']
        password = arg.pop('password')
        user = User(**arg)
        user.set_password(password)
        user.save()
        person = Person(user=user, in_danger=in_danger)
        person.save()
        return person


class UserDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class PersonDetailSerializer(serializers.ModelSerializer):
    user = UserDetailSerializer()

    class Meta:
        model = Person
        fields = ('in_danger', 'user')


class ApproveDelSerializer(serializers.ModelSerializer):
    class Meta:
        model = ApproveRequest


class FileUploadSerializer(serializers.ModelSerializer):
    related_person = PersonDetailSerializer

    class Meta:
        model = FileUpload
        fields = ('image_location', 'related_person')