from django.contrib import admin
from api.models import Travel, Person, SafeContact, Notification, ApproveRequest
# Register your models here.

admin.site.register(Travel)
admin.site.register(Person)
admin.site.register(SafeContact)
admin.site.register(Notification)
admin.site.register(ApproveRequest)